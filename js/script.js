    if (!localStorage.getItem('theme')) {
        localStorage.setItem('theme', 'css/black-white.css');
    }

    const cssStyle = document.getElementById('page-theme');
    let hrefAttr = cssStyle.getAttribute('href');
    let changeBtn = document.getElementById("changeBtn");
    hrefAttr = cssStyle.setAttribute('href', localStorage.getItem('theme'));

    changeBtn.addEventListener('click', changeColor);

    function changeColor() {

        if (localStorage.getItem('theme') === 'css/orange-red.css') {
            localStorage.setItem('theme', 'css/black-white.css');
            cssStyle.setAttribute('href', 'css/black-white.css');
        } else {
            localStorage.setItem('theme', 'css/orange-red.css');
            cssStyle.setAttribute('href', localStorage.getItem('theme'));
        }
    }
